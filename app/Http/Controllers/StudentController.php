<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;
//
//use Intervention\Image\Image;
use Image;

class StudentController extends Controller
{
    public function store(Request $request){

        if ($request->hasFile('image')){
            $images = $request->file('image');
            $filename = time().'.'.$images->getClientOriginalExtension();
            Image::make($images)->resize(300,300)->save(public_path('/uploads/images/'.$filename));

            $objStudentModel = new Student();
            $objStudentModel->name = $_POST["name"];
            $objStudentModel->images = $filename;
            $status = $objStudentModel->save();
            if($status) echo "success!";
            else echo "failed";

            return redirect("students/index");
        }

    }

    public function index() {

        $objStudentModel = new Student();

        $allStudents = $objStudentModel->paginate(3);

        return view('Students/index', compact('allStudents') );
    }


    public function view($id){


        $objStudentModel = new Student();

        $student = $objStudentModel->find($id);

        return view('Students.view',compact('student'));

    }




    public function edit($id){


        $objStudentModel = new Student();

        $student = $objStudentModel->find($id);

        return view('Students.edit',compact('student'));

    }







    public function update(Request $request){


        if ($request->hasFile('image')){
            $images = $request->file('image');
            $filename = time().'.'.$images->getClientOriginalExtension();
            Image::make($images)->resize(300,300)->save(public_path('/uploads/images/'.$filename));

            $objStudentModel = new Student();
            $student = $objStudentModel->find($_POST["id"]);
            $student->name = $_POST["name"];
            $student->images = $filename;
            $status = $student->update();
            if($status) echo "success!";
            else echo "failed";

            return redirect("students/index");
        }
    }



    public function delete($id){


        $objStudentModel = new Student();


        $status = $objStudentModel->find($id)->delete();


        if($status) echo "success!";
        else echo "failed";

        return redirect("students/index");

    }





    public function search($keyword){


        $objStudentModel = new Student();

        $searchResult = $objStudentModel
            ->where("name","LIKE","%$keyword%")
            ->orwhere("images","LIKE","%$keyword%")
            ->paginate(3);



        return view('Students.search_result',compact('searchResult')) ;



    }

}
