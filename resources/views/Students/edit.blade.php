<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<head>

    <style>


        .container{
            position: absolute;
            left: 50px;
            top: 150px;

        }


    </style>

</head>

<div class="container">
    <h1> Edit Students Information</h1>
    <form enctype="multipart/form-data" method="post" action="update">
        <div style="margin: 20px">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id" value="{!! $student["id"] !!}">
            Name: <input type="text" name="name" value="{!! $student["name"] !!}" > <br>
        </div>
        <div style="margin: 20px">
            <label for="image"><b>Image Upload</b></label>
            <img src="{{url('/uploads/images/'.$student['images'])}}" style="width: 70px;height: 60px;border-radius: 50%" alt="Image"/>
            <input type='file' id="image" name="image" accept=".png,.jpg" value="{!! $student["images"] !!}" >
        </div>
        <div class="btn" style="margin: 20px">
            <input type="submit" value="Create" class="btn btn-primary">
        </div>
    </form>

</div>

