<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/students/create',function (){
    return view('Students/create');
});

Route:: post('/students/store','StudentController@store');
Route:: get('students/index','StudentController@index');

Route:: get('/students/view/{id}', ['uses' => 'StudentController@view']);

Route:: get('/students/edit/{id}', ['uses' => 'StudentController@edit']);

Route::post('/students/edit/update','StudentController@update');

Route:: get('/students/delete/{id}', ['uses' => 'StudentController@delete']);

Route::post('/students/search_result',function(){
    $path = '/students/search_result/'.$_POST['keyword'];
    return redirect($path);

});



Route::get('students/search_result/{keyword}', ['uses'=> 'StudentController@search']);

